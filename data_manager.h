#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H

struct data_manager{
        struct fuzzy_vector **training;
        int training_length;

        struct fuzzy_vector **test;
        int test_length;
};

struct data_manager *data_manager_new();

void data_manager_free(struct data_manager *manager);

void data_manager_load_training(struct data_manager *manager, 
                char *source, char *separator, unsigned char test_percent);

void data_manager_randomize_training(struct data_manager *manager);
#endif
