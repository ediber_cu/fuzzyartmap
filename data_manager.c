#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "data_manager.h"
#include "data_reader.h"
#include "fuzzy_vector.h"


struct data_manager *data_manager_new(){
        struct data_manager *result = NULL;

        if ((result = malloc(sizeof(struct data_manager)))){
                result->training = NULL;
                result->training_length = 0;
                result->test = NULL;
                result->test_length = 0;
        }

        return result;
}

void data_manager_free(struct data_manager *manager){
        if (NULL != manager){
                int i;
                for(i = 0; i < manager->training_length; i++){
                        fuzzy_vector_free(manager->training[i]);
                }

                free(manager->training);

                for(i = 0; i < manager->test_length; i++){
                        fuzzy_vector_free(manager->test[i]);
                }

                free(manager->test);

                free(manager);
        }
}

static void fill_data_sets(struct data_manager *manager, 
                struct fuzzy_vector_list *list,
                unsigned char test_percent){

        if (NULL != list){
                // TODO: Divide the list into training/test sets
                int length = fuzzy_vector_list_length(list);
                manager->training_length = length * test_percent / 100;
                manager->test_length = length - manager->training_length; 

                int index;
                struct fuzzy_vector_node *node = fuzzy_vector_list_remove_first(list); 

                if (!(manager->training = malloc(sizeof(struct fuzzy_vector *) * manager->training_length))){
                        // TODO: Exception 
                }
                for(index = 0; NULL != node && index < manager->training_length;
                                node = fuzzy_vector_list_remove_first(list), index++){
                        manager->training[index] = node->data;

                        fuzzy_vector_node_free(node);
                } 

                if (!(manager->test = malloc(sizeof(struct fuzzy_vector *) * manager->test_length))){
                        // TODO: Exception 
                }
                for(index = 0; NULL != node && index < manager->test_length;
                                node = fuzzy_vector_list_remove_first(list), index++){
                        manager->test[index] = node->data;
                        fuzzy_vector_node_free(node);
                } 
        }
}

// TODO: Part of this function is particular to each problem
//       so it should provide extension points to
//       customize the reading (We give the implementation
//       that we need now to handle this specific case)
#define VECTOR_LENGTH 16
#define MAX_VECTOR_VALUE 15
void data_manager_load_training(struct data_manager *manager,
                char *source, char *separator, unsigned char test_percent){
        struct data_reader *reader;
        struct fuzzy_vector *vector;
        struct fuzzy_vector_list *list = NULL;
        char *vclass = NULL;
        int values[VECTOR_LENGTH];

        if ((reader = data_reader_new(source, separator))){
                list = fuzzy_vector_list_new(NULL);
                int vector_length = 2 * VECTOR_LENGTH;
                while(data_reader_next(reader)){
                        int index = 0;
                        if (data_reader_get_string(reader, &vclass)){
                                while(index < VECTOR_LENGTH && 
                                        data_reader_get_int(reader, 
                                                &values[index++]));
                        }

                        if (index == VECTOR_LENGTH){
                                vector = fuzzy_vector_new(vector_length);
                                vector->vclass = vclass;
                                vector->length = vector_length;
                                int i;
                                for(i = 0; i < VECTOR_LENGTH; i++){
                                        // Normalize the vector values to
                                        // [0..1];
                                        vector->values[i] = (double)values[i] / (double)MAX_VECTOR_VALUE;
                                        vector->values[i + VECTOR_LENGTH] =
                                                1 - vector->values[i];
                                }

                                fuzzy_vector_list_add(list, vector);

                        } else {
                                // TODO: Exception or Warning message
                        }
                }

                data_reader_free(reader);

                fill_data_sets(manager, list, test_percent);

                fuzzy_vector_list_free(list);
        }
}

void data_manager_randomize_training(struct data_manager *manager){
        if (NULL == manager){
                // TODO: Exception
        }
        
        srand(time(NULL));
        
        int i;
        for (i = 0; i < manager->training_length; i++){
                int a = rand() % manager->training_length; 
                int b = rand() % manager->training_length; 

                struct fuzzy_vector *tmp = manager->training[a];

                manager->training[a] = manager->training[b];
                manager->training[b] = tmp;
        }
}
