#ifndef ED4F_HASH_H
#define ED4F_HASH_H

#include <stdlib.h>

#include "ed4f_common.h"

struct ed4f_hash_item {
        const char *key;
        void *value;
};

struct ed4f_hash;

struct ed4f_hash_iterator;

struct ed4f_hash_iterator *ed4f_hash_iterator_new(struct ed4f_hash *hash);

void ed4f_hash_iterator_free(struct ed4f_hash_iterator *iterator);

void ed4f_hash_iterator_reset(struct ed4f_hash_iterator *iterator);

struct ed4f_hash_item *ed4f_hash_iterator_next(struct ed4f_hash_iterator *iterator);

typedef size_t (*ed4f_hash_function)(const char *key);

struct ed4f_hash *ed4f_hash_new(ed4f_hash_function hash_function, ed4f_data_free data_free);

size_t ed4f_hash_add(struct ed4f_hash *hash, const char *key, void *value);

struct ed4f_hash_item *ed4f_hash_get(struct ed4f_hash *hash, const char *key);

void ed4f_hash_free(struct ed4f_hash *hash);
#endif
