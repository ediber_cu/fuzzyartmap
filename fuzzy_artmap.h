#ifndef FUZZY_ARTMAP_H
#define FUZZY_ARTMAP_H

#include "data_manager.h"
#include "ed4f_array.h"
#include "ed4f_heap.h"

#define FUZZY_ARTMAP_DEF_CHOICE_PARAM 0.001
#define FUZZY_ARTMAP_DEF_VIGILANCE_PARAM 0.001
#define FUZZY_ARTMAP_DEF_VIGILANCE_PARAM_INC 0.001
// Defaults to Fast Learning
#define FUZZY_ARTMAP_DEF_LEARNING_RATE 1
#define FUZZY_ARTMAP_DEF_TRAINING_ROUNDS 1

struct fuzzy_artmap{
        struct data_manager *manager;        
        double choice_param;
        double vigilance_param;
        double vigilance_param_inc;
        double learning_rate;
        struct ed4f_array *classes;
};

struct fuzzy_artmap_node{
        struct fuzzy_vector_node *w;
        struct fuzzy_vector *iv_and_w;
};

struct fuzzy_artmap_node *fuzzy_art_map_node_new(struct fuzzy_vector_node *w,
                struct fuzzy_vector *iv_and_w);

void fuzzy_art_map_node_free(struct fuzzy_artmap_node *node);

struct fuzzy_artmap *fuzzy_artmap_new(struct data_manager *manager);

void fuzzy_artmap_free(struct fuzzy_artmap *artmap);

/*
 * Performs a training over the training set.
 * This corresponds to 1 epoch.
 */
void fuzzy_artmap_train(struct fuzzy_artmap *artmap);

void fuzzy_artmap_reset_classes(struct fuzzy_artmap *artmap);

/*
 * Evaluates the classes defined in artmap
 * over the all test set.
 * Returns the percentage for correct
 * classifications.
 */
double fuzzy_artmap_test(struct fuzzy_artmap *artmap);

/*
 * Performs the test over a set of data
 * other than the test set
 */
double fuzzy_artmap_test_data(struct fuzzy_artmap *artmap, struct fuzzy_vector **data, int length);

/*
 * Evaluates and individual vector and
 * returns the Heap structure with the
 * selected class at the top.
 */
struct ed4f_heap *fuzzy_artmap_test_vector(struct fuzzy_artmap *artmap,
                struct fuzzy_vector *iv);

#endif
