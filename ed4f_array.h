#ifndef ED4F_ARRAY_H
#define ED4F_ARRAY_H

#include "ed4f_common.h"

struct ed4f_array;

struct ed4f_array *ed4f_array_new(ed4f_data_free data_free);

void ed4f_array_free(struct ed4f_array *array);

void ed4f_array_empty(struct ed4f_array *array);

void **ed4f_array_items(struct ed4f_array *array);

int ed4f_array_length(struct ed4f_array *array);

int ed4f_array_add(struct ed4f_array *array, void *item);

int ed4f_array_set(struct ed4f_array *array, int index, void *item);
#endif
