#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "ed4f_hash.h"
#include "ed4f_string.h"
#include "ed4f_array.h"


struct ed4f_hash{
        ed4f_hash_function hash_function;
        struct ed4f_array *data;
        ed4f_data_free data_free;
};

struct ed4f_hash_iterator{
        struct ed4f_hash *hash;

        size_t current;
};

struct ed4f_hash *ed4f_hash_new(ed4f_hash_function hash_function, ed4f_data_free data_free){
        struct ed4f_hash *result = malloc(sizeof(struct ed4f_hash));

        result->hash_function = hash_function;
        result->data = ed4f_array_new(NULL);
        result->data_free = data_free;

        return result;
}

static struct ed4f_hash_item *ed4f_hash_item_new(const char *key, void *value){
        struct ed4f_hash_item *result = malloc(sizeof(struct ed4f_hash_item));
        
        result->key = ed4f_string_clone(key);

        result->value = value;

        return result;
}

static void ed4f_hash_item_free(struct ed4f_hash_item *item){
        if (NULL == item){
                return;
        }

        free((char *)item->key);
        free(item);
}

size_t ed4f_hash_add(struct ed4f_hash *hash, const char *key, void *value){
        assert(NULL != key && strlen(key) > 0);

        size_t index = hash->hash_function(key);
        
        ed4f_array_set(hash->data, index, ed4f_hash_item_new(key, value));

        return index;
}

struct ed4f_hash_item *ed4f_hash_get(struct ed4f_hash *hash, const char *key){
        if (NULL == hash){
                return NULL;
        }

        assert(NULL != key && strlen(key) > 0);

        size_t index = hash->hash_function(key);
        size_t length = ed4f_array_length(hash->data);

        if(index >= length){
                return NULL;
        }

        void **items = ed4f_array_items(hash->data);

        return (struct ed4f_hash_item *)items[index];
}

void ed4f_hash_free(struct ed4f_hash *hash){

        void **items = ed4f_array_items(hash->data);
        
        for (int i = 0; i < ed4f_array_length(hash->data); i++){
                if (NULL != hash->data_free){
                        hash->data_free(((struct ed4f_hash_item *)items[i])->value);
                }

                ed4f_hash_item_free((struct ed4f_hash_item *)items[i]);
        }

        ed4f_array_free(hash->data);

        free(hash);
}

struct ed4f_hash_iterator *ed4f_hash_iterator_new(struct ed4f_hash *hash){
        struct ed4f_hash_iterator *result = malloc(sizeof(struct ed4f_hash_iterator));

        result->hash = hash;

        result->current = 0;

        return result;
}

void ed4f_hash_iterator_free(struct ed4f_hash_iterator *iterator){
        free(iterator);
}

struct ed4f_hash_item *ed4f_hash_iterator_next(struct ed4f_hash_iterator *iterator){
        struct ed4f_hash_item *result = NULL;
        void **items = ed4f_array_items(iterator->hash->data);

        size_t index = iterator->current;
        size_t length = ed4f_array_length(iterator->hash->data);
        while(index < length &&
                        (NULL == (result = (struct ed4f_hash_item *)items[index]))){
                index++;
        }

        iterator->current = index + 1;

        return result;
}

void ed4f_hash_iterator_reset(struct ed4f_hash_iterator *iterator){
        iterator->current = 0;
}
