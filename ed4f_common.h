#ifndef ED4F_COMMON_H
#define ED4F_COMMON_H

#include <stdbool.h>

typedef void (*ed4f_data_free)(void *data);

bool double_max_ordered(double a, double b);
#endif
