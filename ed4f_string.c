#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "ed4f_string.h"

char *ed4f_string_clone(const char *str){
        assert(NULL != str);

        size_t length = strlen(str);

        char *clone = malloc(sizeof(char) * (length + 1));

        strcpy(clone, str);

        return clone;
}
