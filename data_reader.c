#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data_reader.h"


struct data_reader{
        FILE  *file;
        unsigned char is_open;
        struct data_reader_record *current;
        char *separator;
};

struct data_reader_record{
        char **items;
        size_t length;
        int index;
};

/********************************
 * Function to handle data_reader
 ********************************/

struct data_reader  * data_reader_new(char *source, char *separator){
        struct data_reader *result = NULL;
        
        if ((result = malloc(sizeof(struct data_reader)))){
                if ((result->file = fopen(source, "r"))){
                        result->is_open = true;
                        result->current = NULL;
                        result->separator = separator;
                } else {
                        // TODO: Exception
                        result->is_open = false;
                }
        } else {
                // TODO: Exception
        }

        return result;
}

void data_reader_free(struct data_reader *reader){
        if (NULL != reader){
                if (reader->is_open){
                        fclose(reader->file);
                } 

                free(reader);
        }
}

/****************************************
 * Functions to handle data_reader_record
 ****************************************/
#define READ_BUFFER_SIZE 1024

static bool init_record(char *separator, char * buffer, struct data_reader_record **record){
        char *token;
        char *sptr;
        char * tokens[READ_BUFFER_SIZE];
        bool result = false;

        size_t index = 0;
        
        token = strtok_r(buffer, separator, &sptr);

        while(NULL != token){
                size_t length = strlen(token) + 1;  

                tokens[index] = malloc(sizeof(char) * length);        
                strcpy(tokens[index], token);

                index++;

                token = strtok_r(NULL, separator, &sptr);
        }

        if (index > 0 && (*record = malloc(sizeof(struct data_reader_record)))){
                if (((*record)->items = malloc(sizeof(char *) * index))){
                        int i;
                        for(i = 0; i < index; i++){
                                (*record)->items[i] = tokens[i];
                        }
                        
                        (*record)->index = 0;
                        (*record)->length = index;

                        result = true;
                }
        }

        return result;
}

static void data_reader_record_free(struct data_reader_record *record){
        if (record != NULL){
                int i;
                for (i = 0; i < record->length; i++){
                        free(record->items[i]);
                }

                free(record->items);

                free(record);

                record = NULL;
        }
}

bool data_reader_next(struct data_reader * reader){
        bool result = false;
        struct data_reader_record *record = NULL;
        char buffer[READ_BUFFER_SIZE];

        if (NULL != reader){
                // Destroy previous record 
                if (NULL != reader->current){
                        data_reader_record_free(reader->current);
                }

                if ((!fgets(buffer, READ_BUFFER_SIZE - 1, reader->file))){
                        return false;
                }

                size_t length = strlen(buffer);

                // Removing EOF from reading
                if ('\n' == buffer[length - 1]){
                        buffer[--length] = '\0';
                }

                if (length > 0 && init_record(reader->separator, buffer, &record)){
                        // Set the record as the current record
                        // in the reader
                        reader->current = record;
                        
                        // Indicate that a new record was
                        // created
                        result = true;
                }
        } else {
                // TODO: Exception
        }

        return result;
}

bool data_reader_get_string(struct data_reader *reader, char **str){
        *str = NULL;
        bool result = false;

        if (NULL != reader && NULL != reader->current && 
                        reader->current->index < reader->current->length){
                if ((*str = malloc(sizeof(char) * (reader->current->length + 1)))){
                        strcpy(*str, reader->current->items[reader->current->index++]); 

                        result = true;
                } else {
                        // TODO: Exception
                }
        }

        return result;
}

bool data_reader_get_int(struct data_reader *reader, int *data){
        *data = 0;
        bool result = false;

        if (NULL != reader && NULL != reader->current && 
                        reader->current->index < reader->current->length){
                *data = atoi(reader->current->items[reader->current->index++]);

                result = true;
        }

        return result;
}

bool data_reader_get_double(struct data_reader *reader, double *data){
        *data = 0;
        bool result = false;

        if (NULL != reader && NULL != reader->current && 
                        reader->current->index < reader->current->length){
                *data = atof(reader->current->items[reader->current->index++]);
                result = true;
        }

        return result;
}

size_t data_reader_get_record_length(struct data_reader *reader){
        return NULL != reader && NULL != reader->current ?
                reader->current->length : 0;
}
