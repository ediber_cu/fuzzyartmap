#ifndef ED4F_STRING_H
#define ED4F_STRING_H

/*
 * Returns a copy of the string
 */
char *ed4f_string_clone(const char *str);
#endif
