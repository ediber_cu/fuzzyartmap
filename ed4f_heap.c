#include <stdlib.h>
#include <assert.h>

#include "ed4f_heap.h"

#define ED4F_HEAP_INCREASE_RATE 50
#define ED4F_HEAP_DEF_LENGTH 10

struct ed4f_heap{
        struct ed4f_heap_node **items;
        int length;
        int max_length;
        double_ordered ordered;
};

struct ed4f_heap_node *ed4f_heap_node_new(void *item, double value){
        struct ed4f_heap_node *node = malloc(sizeof(struct ed4f_heap_node));
        
        node->data = item;
        node->value = value;

        return node;
}

void ed4f_heap_node_free(struct ed4f_heap_node *node){
        free(node);
}

struct ed4f_heap *ed4f_heap_new(double_ordered order){
        struct ed4f_heap *heap = malloc(sizeof(struct ed4f_heap));

        heap->ordered = order;
        heap->items = malloc((sizeof(struct ed4f_heap_node *) * ED4F_HEAP_DEF_LENGTH));
        heap->max_length = ED4F_HEAP_DEF_LENGTH;
        heap->length = 0;

        return heap;
}

void ed4f_heap_free(struct ed4f_heap *heap){
        if (NULL != heap){
                int i;
                for (i = 0; i < heap->length; i++){
                        ed4f_heap_node_free(heap->items[i]);
                }

                free(heap->items);
                free(heap);
        }
}

void ed4f_heap_clear(struct ed4f_heap *heap){
        if (NULL != heap){
                int i;
                for (i = 0; i < heap->length; i++){
                        ed4f_heap_node_free(heap->items[i]);
                }
                
                heap->length = 0;
        }
}

static void swap_nodes(struct ed4f_heap *heap, int a, int b){
        struct ed4f_heap_node *tmp = heap->items[a];
        heap->items[a] = heap->items[b];
        heap->items[b] = tmp;
}

static void push_up(struct ed4f_heap *heap, int index){
        int p_index; 

        for(p_index = (index + 1) / 2 - 1; p_index >= 0 && 
                        !heap->ordered(heap->items[p_index]->value,
                                heap->items[index]->value);
                        p_index = (index + 1) / 2 - 1){

                swap_nodes(heap, p_index, index);

                index = p_index;
        }
}

static void push_down(struct ed4f_heap *heap, int index){
        bool stop = heap->length <= 1; // No need to reorder when only one
        int c, tmp;
        
        while(!stop)
        {
                // test agains left child
                tmp = 2 * index + 1; 
                c =  tmp < heap->length && 
                        heap->ordered(heap->items[tmp]->value, 
                                        heap->items[index]->value) ? tmp : index;
                // test agains right child
                tmp++;
                c =  tmp < heap->length && 
                        heap->ordered(heap->items[tmp]->value, 
                                        heap->items[c]->value) ? tmp : c;

                if (c != index){
                        swap_nodes(heap, c, index);
                } else {
                        stop = true;
                }
        }
}

void ed4f_heap_push(struct ed4f_heap *heap, void *item, double value){
        // Increase the array size if needed
        if (heap->length >= heap->max_length){
                heap->max_length += heap->max_length * ED4F_HEAP_INCREASE_RATE / 100; 
                heap->items = realloc(heap->items, sizeof(void *) * heap->max_length);
        } 

        heap->items[heap->length++] = ed4f_heap_node_new(item, value);
        
        push_up(heap, heap->length - 1);
}

struct ed4f_heap_node *ed4f_heap_pop(struct ed4f_heap *heap){
        struct ed4f_heap_node *result = NULL; 

        if (heap->length > 0){
                result = heap->items[0]; 
                heap->items[0] = heap->items[--(heap->length)];

                push_down(heap, 0);
        }

        return result;
}

struct ed4f_heap_node *ed4f_heap_top(struct ed4f_heap *heap){
        if (heap->length > 0){
                return heap->items[0];
        } else {
                return NULL;
        }
}

size_t ed4f_heap_length(struct ed4f_heap *heap){
        assert(NULL != heap);

        return heap->length;
}
