#include <stdlib.h>

#include "test_result.h"


struct test_result *test_result_new(double vigilance_param, int class_count, double matches){
        struct test_result *result = malloc(sizeof(struct test_result));

        result->vigilance_param = vigilance_param;
        result->class_count = class_count;
        result->matches = matches; 

        return result;
}

void test_result_free(struct test_result *result){
        free(result);
}
