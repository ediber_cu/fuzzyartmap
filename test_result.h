#ifndef TEST_RESULT_H
#define TEST_RESULT_H

struct test_result{
        double vigilance_param;
        int class_count;
        double matches;
};

struct test_result *test_result_new(double vigilance_param, int class_count, double matches);

void test_result_free(struct test_result *result);
#endif
