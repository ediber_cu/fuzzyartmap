#ifndef DATA_READER_H
#define DATA_READER_H

#include <stdlib.h>
#include <stdbool.h>

struct data_reader;

struct data_reader_record;

struct data_reader * data_reader_new(char *source, char *separator);

void data_reader_free(struct data_reader *reader);

bool data_reader_next(struct data_reader * reader);

bool data_reader_get_string(struct data_reader *reader, char **str);

bool data_reader_get_double(struct data_reader *reader, double *data);

bool data_reader_get_int(struct data_reader *reader, int *data);

size_t data_reader_get_record_length(struct data_reader *reader);
#endif
