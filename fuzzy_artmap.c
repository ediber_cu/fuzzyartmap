#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "ed4f_common.h"
#include "fuzzy_artmap.h"
#include "fuzzy_vector.h"
#include "ed4f_heap.h"

static void fuzzy_vector_data_free(void *data){
        fuzzy_vector_free((struct fuzzy_vector *)data);
}

struct fuzzy_artmap_node *fuzzy_art_map_node_new(struct fuzzy_vector_node *w,
                struct fuzzy_vector *iv_and_w){
        struct fuzzy_artmap_node *result = malloc(sizeof(struct fuzzy_artmap_node));

        result->w = w;
        result->iv_and_w = iv_and_w;

        return result;
}

void fuzzy_art_map_node_free(struct fuzzy_artmap_node *node){
        if (NULL != node){
                free(node);
        }
}

struct fuzzy_artmap *fuzzy_artmap_new(struct data_manager *manager){
        struct fuzzy_artmap *artmap = malloc(sizeof(struct fuzzy_artmap));
        
        artmap->manager = manager;
        artmap->choice_param = FUZZY_ARTMAP_DEF_CHOICE_PARAM;
        artmap->vigilance_param = FUZZY_ARTMAP_DEF_VIGILANCE_PARAM;
        artmap->vigilance_param_inc = FUZZY_ARTMAP_DEF_VIGILANCE_PARAM_INC;
        artmap->learning_rate = FUZZY_ARTMAP_DEF_LEARNING_RATE; 

        artmap->classes = ed4f_array_new(fuzzy_vector_data_free);

        return artmap;
}

void fuzzy_artmap_free(struct fuzzy_artmap *artmap){
        if (NULL != artmap){
                ed4f_array_free(artmap->classes);
                free(artmap);
        }
}

void fuzzy_artmap_reset_classes(struct fuzzy_artmap *artmap){
        if (NULL != artmap){
                ed4f_array_free(artmap->classes);
                artmap->classes = ed4f_array_new(fuzzy_vector_data_free);
        }
}

struct ed4f_heap *fuzzy_artmap_test_vector(struct fuzzy_artmap *artmap,
                struct fuzzy_vector *iv){

        void **items = ed4f_array_items(artmap->classes);

        struct ed4f_heap *heap = ed4f_heap_new(double_max_ordered);
        size_t length = ed4f_array_length(artmap->classes);

        double iv_norm = fuzzy_vector_norm(iv);
        double vp = artmap->vigilance_param;
        double cp = artmap->choice_param;

        for (int i = 0; i < length; i++){
                struct fuzzy_vector *w = (struct fuzzy_vector *)items[i];
                // iv_and_w is a new vector (iv not changed)
                struct fuzzy_vector *iv_and_w = fuzzy_vector_and(iv, w, false);
                double iv_and_w_norm = fuzzy_vector_norm(iv_and_w);

                // This vector is no longer needed, only it's norm 
                fuzzy_vector_free(iv_and_w);

                // Only consider values >= vigilance parameter;
                // If training we can also discard values that don't
                // match the expected class
                if (iv_and_w_norm / iv_norm >= vp){
                        double w_norm = fuzzy_vector_norm(w);
                        double categ_choice = iv_and_w_norm / (cp + w_norm);

                        ed4f_heap_push(heap, w, categ_choice);
                }
        }

        return heap;
}

double fuzzy_artmap_test_data(struct fuzzy_artmap *artmap, struct fuzzy_vector **data, int length){
        int match_count = 0; 

        for (int i = 0; i < length; i++){
                struct fuzzy_vector *iv = data[i];
                struct ed4f_heap *heap = fuzzy_artmap_test_vector(artmap, iv);

                struct ed4f_heap_node *node = ed4f_heap_pop(heap);

                if (NULL != node){
                        if (0 == strcmp(((struct fuzzy_vector *)node->data)->vclass,
                                        iv->vclass)){
                                match_count++;
                        }
                        ed4f_heap_node_free(node);
                }

                ed4f_heap_free(heap);
        }
        
        return (double)match_count / (double)length * (double)100;
}

double fuzzy_artmap_test(struct fuzzy_artmap *artmap){

        struct data_manager *manager = artmap->manager;
        int input_length =  manager->test_length;
        struct fuzzy_vector **input_data = manager->test;

        return fuzzy_artmap_test_data(artmap, input_data, input_length);
}

static struct ed4f_heap_node *fuzzy_artmap_match_tracking(
                struct fuzzy_artmap *artmap,
                struct ed4f_heap *heap,
                struct fuzzy_vector *iv){

        assert(NULL != artmap);
        assert(NULL != heap);
        assert(NULL != iv);

        double vp = artmap->vigilance_param; 
        struct ed4f_heap_node *node = NULL;
        double iv_norm = fuzzy_vector_norm(iv);

        node = ed4f_heap_pop(heap); // This is not empty
        struct fuzzy_vector *w = (struct fuzzy_vector *)node->data;
        struct fuzzy_vector *iv_and_w = fuzzy_vector_and(iv, w, false);
        double iv_and_w_norm = fuzzy_vector_norm(iv_and_w);
        fuzzy_vector_free(iv_and_w);

        
        double v = (double)iv_and_w_norm / (double)iv_norm;

        do{
                // Increment the vigilance parameter
                vp = v + artmap->vigilance_param_inc;

                do{ // Search for the next vector
                        ed4f_heap_node_free(node);
                        node = ed4f_heap_pop(heap);
                        
                        if (NULL != node){
                                w = (struct fuzzy_vector *)node->data;
                                iv_and_w = fuzzy_vector_and(iv, w, false);
                                iv_and_w_norm = fuzzy_vector_norm(iv_and_w);
                                fuzzy_vector_free(iv_and_w);
                                v = (double)iv_and_w_norm / (double)iv_norm;
                        }
                } while (NULL != node && v > vp);
        } while(NULL != node && 0 != strcmp(
                        ((struct fuzzy_vector *)node->data)->vclass,
                        iv->vclass));

        return node;
}

void fuzzy_artmap_train(struct fuzzy_artmap *artmap){
        assert(NULL != artmap);

        struct data_manager *manager = artmap->manager;
        int input_length =  manager->training_length;
        struct fuzzy_vector **input_data = manager->training;

        for (int j = 0; j < input_length; j++){
                struct fuzzy_vector *iv = input_data[j];

                struct ed4f_heap *heap = fuzzy_artmap_test_vector(artmap, iv);
                
                struct ed4f_heap_node *node = ed4f_heap_top(heap);

                if (NULL != node) {
                        struct fuzzy_vector *w = (struct fuzzy_vector *)node->data;

                        if (0 != strcmp(w->vclass, iv->vclass)) { // No match
                                // Start match tracking
                                node = fuzzy_artmap_match_tracking(artmap, heap, iv);

                                if (NULL != node){ // Match found
                                        // Learn (Update the vector w with (w ^ iv))
                                        fuzzy_vector_and(w, iv, true);

                                        ed4f_heap_node_free(node);
                                } else { // Empty or no match
                                        ed4f_array_add(artmap->classes, fuzzy_vector_clone(iv)); 
                                }
                        } else { // Learn
                                // Learn (Update the vector w with (w ^ iv))
                                fuzzy_vector_and(w, iv, true);
                        }
                        
                } else { // Empty or no match
                        ed4f_array_add(artmap->classes, fuzzy_vector_clone(iv)); 
                }
                
                ed4f_heap_free(heap);
        }
}
