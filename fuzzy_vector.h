#ifndef FUZZY_VECTOR_H
#define FUZZY_VECTOR_H

#include <stdbool.h>

#include "ed4f_common.h"

struct fuzzy_vector{
        double *values;
        int length;
        char *vclass;
};

struct fuzzy_vector_node{
        struct fuzzy_vector *data;
        struct fuzzy_vector_node *next;
};

struct fuzzy_vector_list{
        struct fuzzy_vector_node *first;
        struct fuzzy_vector_node *last;
        ed4f_data_free data_free;
        int length;
};

struct fuzzy_vector * fuzzy_vector_new(int length);

void fuzzy_vector_free(struct fuzzy_vector *vector);

struct fuzzy_vector *fuzzy_vector_clone(struct fuzzy_vector *vector);

/*
 * Performs the fuzzy AND opperation between two vectors.
 * The last parameter (result_in_a) indicates whether we need
 * to create a new vector to store the results or update the
 * vector (a) with the result of the operation.
 */
struct fuzzy_vector *fuzzy_vector_and(struct fuzzy_vector *a, struct fuzzy_vector *b, bool result_in_a);

/*
 * Calculates the norm for the vector.
 * The norm is defined as the sum of each of its
 * components
 */
double fuzzy_vector_norm(struct fuzzy_vector *vector);

struct fuzzy_vector_node *fuzzy_vector_node_new(struct fuzzy_vector * vector);

void fuzzy_vector_node_free(struct fuzzy_vector_node *node);

struct fuzzy_vector_list *fuzzy_vector_list_new(ed4f_data_free data_free);

void fuzzy_vector_list_free(struct fuzzy_vector_list *list);

bool fuzzy_vector_list_add(struct fuzzy_vector_list *list, struct fuzzy_vector * vector);

struct fuzzy_vector_node *fuzzy_vector_list_remove_first(struct fuzzy_vector_list *list);

struct fuzzy_vector_node *fuzzy_vector_list_first(struct fuzzy_vector_list *list);

int fuzzy_vector_list_length(struct fuzzy_vector_list *list);

#endif
