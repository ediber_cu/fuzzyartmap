#include <stdlib.h>

#include "ed4f_array.h"

#define ED4F_ARRAY_INCREASE_RATE 25
#define ED4F_ARRAY_DEF_LENGTH 10

struct ed4f_array{
        void **items;
        size_t length;
        size_t max_length;
        ed4f_data_free data_free; 
};

struct ed4f_array *ed4f_array_new(ed4f_data_free data_free){
        struct ed4f_array *array = malloc(sizeof(struct ed4f_array));

        array->items = malloc((sizeof(void *) * ED4F_ARRAY_DEF_LENGTH));
        array->max_length = ED4F_ARRAY_DEF_LENGTH;
        array->data_free = data_free;

        // TODO: handle this with a memset
        for (int i = 0; i < array->max_length; i++){
                array->items[i] = 0;
        }

        array->length = 0;

        return array;
}

void ed4f_array_free(struct ed4f_array *array){
        if (NULL != array){
                if (NULL != array->data_free){
                        for(int i = 0; i < array->length; i++){
                                array->data_free(array->items[i]);
                        }
                }

                free(array->items);
                free(array);
        }
}

void ed4f_array_empty(struct ed4f_array *array){
        array->length = 0;
}

void **ed4f_array_items(struct ed4f_array *array){
        return array != NULL ? array->items : NULL;
}

int ed4f_array_length(struct ed4f_array *array){
        return array != NULL ? array->length : 0;
}

/*
 * This function increases the array size, if needed, to store the
 * index value "index"
 */
static void ensure_size(struct ed4f_array *array, size_t index){
        // TODO: Add validations

        
        if (index + 1 >= array->max_length){

                size_t tlength = array->max_length;

                // Set the default increase (this handles the cases where adding
                // at the last position)
                array->max_length += array->max_length * ED4F_ARRAY_INCREASE_RATE / 100; 

                // if not enough
                if (index + 1 >= array->max_length){
                        array->max_length = index + 1;
                }  

                array->items = realloc(array->items, sizeof(void *) * array->max_length);

                // TODO: handle this with a memset
                for (int i = tlength; i < array->max_length; i++){
                        array->items[i] = 0;
                }
        }
}

int ed4f_array_set(struct ed4f_array *array, int index, void *item){
        // Ensure that there is space to store the value
        // at position "index"
        ensure_size(array, index);

        if (index >= array->length){
                array->length = index + 1;
        } 

        array->items[index] = item;

        return index;
}

int ed4f_array_add(struct ed4f_array *array, void *item){
        int index = ed4f_array_set(array, array->length, item);

        return index;
}

