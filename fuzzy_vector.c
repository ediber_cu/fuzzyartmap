#include <stdlib.h>
#include <string.h>

#include "fuzzy_vector.h"
#include "ed4f_string.h"

struct fuzzy_vector * fuzzy_vector_new(int length){
        struct fuzzy_vector *result = NULL;

        if ((result = malloc(sizeof(struct fuzzy_vector)))){
               result->values = malloc(sizeof(double) * length);  
               result->vclass = NULL;
               result->length = length;
        } else {
                // TODO: Exception
        }

        return result;
}


void fuzzy_vector_free(struct fuzzy_vector *vector){
        if (NULL != vector){
                if (NULL != vector->values){
                        free(vector->values);
                }

                if (NULL != vector->vclass){
                        free(vector->vclass);
                }

                free(vector);
        }
}

struct fuzzy_vector *fuzzy_vector_clone(struct fuzzy_vector *vector){
        if (NULL == vector){
                return NULL;
        }

        struct fuzzy_vector *result = fuzzy_vector_new(vector->length);
        
        for(int i = 0; i <vector->length; i++){
                // Replace this with a memcopy if apply
                result->values[i] = vector->values[i];
        }

        if (NULL == vector->vclass){
                result->vclass = NULL;
        } else {
                result->vclass = ed4f_string_clone(vector->vclass);
        }

        return result;
}

struct fuzzy_vector *fuzzy_vector_and(struct fuzzy_vector *a, struct fuzzy_vector *b, bool result_in_a){
        if (NULL == a || NULL == b){
                return NULL;
        }
        
        int length = a->length < b->length ? a->length : b->length; 
        struct fuzzy_vector *r = result_in_a ? a : fuzzy_vector_new(length);

        int i;
        for(i = 0; i < length; i++){
                r->values[i] = a->values[i] < b->values[i] ?
                        a->values[i] : b->values[i]; 
        }

        return r;
}

double fuzzy_vector_norm(struct fuzzy_vector *vector){
        if (NULL == vector){
                return 0; // TODO: throw an exception
        }

        double result = 0;
        int i;

        for(i = 0; i < vector->length; i++){
                result += vector->values[i];
        }
       
        return result;
}

struct fuzzy_vector_node *fuzzy_vector_node_new(struct fuzzy_vector * vector){
        struct fuzzy_vector_node *result = NULL;

        if ((result = malloc(sizeof(struct fuzzy_vector_node)))){
                result->data = vector;      
                result->next = NULL;
        }

        return result;
}

void fuzzy_vector_node_free(struct fuzzy_vector_node *node){
        if (NULL != node){
                free(node);
        } 
}

struct fuzzy_vector_list *fuzzy_vector_list_new(ed4f_data_free data_free){
        struct fuzzy_vector_list *result = NULL;

        if ((result = malloc(sizeof(struct fuzzy_vector_list)))){
                result->first = NULL;
                result->last = NULL;
                result->length = 0;
                result->data_free = data_free;
        }

        return result;
}

void fuzzy_vector_list_free(struct fuzzy_vector_list *list){
        if (NULL != list){
                struct fuzzy_vector_node *curr = list->first;
                struct fuzzy_vector_node *next;

                for(; NULL != curr; curr = next){
                        next = curr->next;
                        if (NULL != list->data_free){
                                list->data_free(curr->data);
                        }

                        fuzzy_vector_node_free(curr);
                }

                free(list);
        }
}

bool fuzzy_vector_list_add(struct fuzzy_vector_list *list, struct fuzzy_vector * vector){
        if (NULL != list && NULL != vector){
                struct fuzzy_vector_node *node = fuzzy_vector_node_new(vector); 

                if (NULL == list->last){
                        list->last = list->first = node;
                } else {
                        list->last->next = node;
                        list->last = list->last->next;
                }

                list->length++;
        } else {
                return false;
        }

        return true;      
}

int fuzzy_vector_list_length(struct fuzzy_vector_list *list){
        return NULL != list ? list->length : 0;
}

struct fuzzy_vector_node *fuzzy_vector_list_remove_first(struct fuzzy_vector_list *list){
        if (NULL == list){
                return NULL;
        }

        struct fuzzy_vector_node *first = list->first;

        if (NULL != first){
                list->first = list->first->next; 
                list->length--;

                if (NULL == list->first){
                        list->last = NULL;
                }
        }

        return first;
}

struct fuzzy_vector_node *fuzzy_vector_list_first(struct fuzzy_vector_list *list){
        return NULL != list ? list->first : NULL;
}
