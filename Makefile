package         ?= "fuzzyartmap"
export package

CFLAGS		?= -std=gnu99 -g -O0 -Wall

includes	= $(wildcard *.h)
sources		= $(wildcard *.c) 

objects		= $(subst .c,.o, $(sources))

all: $(package)

$(package): ${objects}
	$(CC) $(CFLAGS) -o $@ ${objects}

$(objects): ${sources} ${includes}
	$(CC) -I./ $(CFLAGS) -c $^

clean:
	-rm -f $(package) >/dev/null 2>&1
	-rm -f *.o *.gch >/dev/null 2>&1

.PHONY: all clean
