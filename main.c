#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data_manager.h"
#include "fuzzy_vector.h"
#include "fuzzy_artmap.h"
#include "ed4f_array.h"
#include "test_result.h"

#define TRAININGS_COUNT 1000

static void main_test_result_free(void *data){
        test_result_free((struct test_result *)data);
}


void write_vector(struct fuzzy_vector *vector){
        printf("\t[%s]  = [", vector->vclass);
        for(int i = 0; i < vector->length; i++){
                printf(", %lf", vector->values[i]);
        }
        printf("]\n");
}

void print_vector_classes(struct fuzzy_artmap *artmap){
        void **items = ed4f_array_items(artmap->classes);
        size_t length = ed4f_array_length(artmap->classes);

        for (int i = 0; i < length; i++){
                struct fuzzy_vector *w = (struct fuzzy_vector *)items[i];
                write_vector(w);
        }
}

int get_classes_length(struct fuzzy_artmap *artmap){
        return ed4f_array_length(artmap->classes);
}

void write_results(struct ed4f_array *data, char *file){
        FILE *f = fopen(file, "w");

        int length = ed4f_array_length(data);
        void **items = ed4f_array_items(data);

        for (int i = 0; i < length; i++ ){
                struct test_result *item = (struct test_result *)items[i];
                fprintf(f, "%lf, %lf, %d\n", item->vigilance_param, item->matches, item->class_count);
        }

        fclose(f);
}

void incremental_VP_test(){
        struct data_manager *manager = data_manager_new();

        data_manager_load_training(manager, "./data/letter-recognition.data", ",", 80);

        printf("Items loaded = %d; for training: %d; for testing=%d\n", 
                        manager->training_length + manager->test_length, 
                        manager->training_length, manager->test_length);

        struct fuzzy_artmap *artmap = fuzzy_artmap_new(manager);


        struct ed4f_array *array = ed4f_array_new(main_test_result_free);

        double max_matches = 0;
        int max_count = 0;
        double max_vp = 0;

        artmap->vigilance_param = 0.1;
        artmap->vigilance_param_inc = 0.1;

        int i = 0;
        while(artmap->vigilance_param <= 1){

                fuzzy_artmap_reset_classes(artmap);
        
                fuzzy_artmap_train(artmap);
                double matches = fuzzy_artmap_test(artmap);
                int count = get_classes_length(artmap);

                ed4f_array_add(array, test_result_new(artmap->vigilance_param, count, matches));

                if (matches > max_matches){
                        max_matches = matches;
                        max_count = count;
                        max_vp = artmap->vigilance_param;
                }

                //print_classes(artmap, 1, matches);
                printf("Iteration [%d] results: [matches] = %lf; [vigilance] = %lf; [classes] = %d\n",
                                i, matches, artmap->vigilance_param, count);

                // Increase the vp parameter;
                artmap->vigilance_param +=artmap->vigilance_param_inc;

                i++;
        }

        //print_classes(artmap, 1, matches);
        printf("Best results: [matches] = %lf; [vigilance] = %lf; [classes] = %d\n", max_matches, max_vp, max_count);

        write_results(array, "incremental_test.txt");

        fuzzy_artmap_free(artmap);

        data_manager_free(manager);

        ed4f_array_free(array);
}

/*
 * Parameters:
 * epoch        = Number of repetittions over the training set
 * vp           = vigilance parameter [0,1]
 * cp           = choice parameter (0, infinite) usualy [0,10]
 * vpi          = vigilance parameter increment on match tracking
 *
 */
void random_order_test(int epoch, double vp, double cp, double vpi, bool testset){
        struct data_manager *manager = data_manager_new();

        data_manager_load_training(manager, "./data/letter-recognition.data.short", ",", 80);

        struct fuzzy_vector **test = NULL;
        int test_length = 0;

        if (testset){
                test = manager->test;
                test_length = manager->test_length;
        } else {
                test = manager->training;
                test_length = manager->training_length;
        }

        printf("Items loaded = %d; for training: %d; for testing=%d\n", 
                        manager->training_length + manager->test_length, 
                        manager->training_length, test_length);

        struct fuzzy_artmap *artmap = fuzzy_artmap_new(manager);

        artmap->vigilance_param = vp;
        artmap->choice_param = cp;
        artmap->vigilance_param_inc = vpi;

        for (int i = 0; i < epoch; i++){
                data_manager_randomize_training(manager);

                printf("Beginning epoch [%d]\n", i); 
                fuzzy_artmap_train(artmap);
                printf("Classes [%d]\n", get_classes_length(artmap)); 

                printf("Beginning Testing\n"); 

                double matches =0;
                
                if (testset){
                        matches = fuzzy_artmap_test(artmap);
                } else {
                        matches = fuzzy_artmap_test_data(artmap, test, test_length);
                }

                int count = get_classes_length(artmap);

                printf("Results:[epoch] = %d; [classes] = %d; [matches] = %lf; [vigilance] = %lf; [choice] = %lf; [increment] = %lf\n", i, count, matches, vp, cp, vpi);
        }

        data_manager_free(manager);

        fuzzy_artmap_free(artmap);
}

int main(int argc, char **argv){

        //incremental_VP_test();

        //random_order_test(2, 0, 0.001, 0.0001, true);

        //random_order_test(2, 0, 0.1, 0.0001, true);

        //random_order_test(2, 0, 1, 0.0001, true);

        random_order_test(6, 0, 5, 0.0001, false);

        //random_order_test(1, 0, 1, 0.001, true);

        return 0;
}
