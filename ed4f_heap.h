#ifndef ED4F_HEAP_H
#define ED4F_HEAP_H

#include <stdlib.h>
#include <stdbool.h>

// Function to determine the heap behaviour (Max or Min);
typedef bool(*double_ordered)(double a, double b);

struct ed4f_heap;

struct ed4f_heap_node{
        void *data;
        double value;
};

struct ed4f_heap_node *ed4f_heap_node_new(void *item, double value);

void ed4f_heap_node_free(struct ed4f_heap_node *node);

struct ed4f_heap *ed4f_heap_new(double_ordered order);

void ed4f_heap_free(struct ed4f_heap *heap);

void ed4f_heap_clear(struct ed4f_heap *heap);

void ed4f_heap_push(struct ed4f_heap *heap, void *item, double value);

struct ed4f_heap_node *ed4f_heap_pop(struct ed4f_heap *heap);

struct ed4f_heap_node *ed4f_heap_top(struct ed4f_heap *heap);

size_t ed4f_heap_length(struct ed4f_heap *heap);
#endif
